# Cookbook Name:: etcd
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

bash "install_etcd" do
  user "root"
  cwd "/tmp"
  code <<-EOH
  curl -L  https://github.com/coreos/etcd/releases/download/v0.4.6/etcd-v0.4.6-linux-amd64.tar.gz -o etcd-v0.4.6-linux-amd64.tar.gz
  tar xzvf etcd-v0.4.6-linux-amd64.tar.gz
  cp etcd-v0.4.6-linux-amd64/etcd /bin/etcd
  mkdir /etcdMachines
  chmod 775 /etcdMachines


  echo '
exec 2> /tmp/rc.local.log      # send stderr from rc.local to a log file
exec 1>&2                      # send stdout to the same log file
set -x                         # tell sh to display commands before execution

myIp=$(curl http://169.254.169.254/latest/meta-data/public-ipv4)
myName=$(curl http://instance-data/latest/meta-data/public-hostname)

etcd -peer-addr $myIp:7001 -addr $myIp:4001 -bind-addr 0.0.0.0:4001 -peer-bind-addr 0.0.0.0:7001 -peers satellite.t-bob.co.uk:7001 -data-dir /etcdMachines -name $myName
exit 0
' >> /etc/rc.local
EOH
end
